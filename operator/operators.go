package main

import (
	"log"
	"fmt"
	"math"
	"math/big"
	"time"
	"os"
	"context"

	"database/sql"
    _ "github.com/mattn/go-sqlite3"

	"github.com/ethereum/go-ethereum/ethclient"
	"github.com/ethereum/go-ethereum/common"

	. "github.com/logrusorgru/aurora"
	"github.com/Arafatk/glot"
)

const (
	// name for DB file on disk which stores historical ETH balance data
	DB_FILE = "balances.db"

	// needed to be able to spawn a new DB if it's not found on disk
	DB_SCHEMA = `CREATE TABLE balances (
                    id INTEGER PRIMARY KEY AUTOINCREMENT,
                    address TEXT NOT NULL,
                    ethbalance INTEGER NOT NULL,
                    timestamp INTEGER NOT NULL
                );`
)

// global variable for DB
var db *sql.DB

// global variable for the ETH RPC client (built-in to ethereum-go)
var client *ethclient.Client

// map for resolving names to ETH public key addresses
var ADDRESS_BOOK = map[string]string{
 	"REC-1":    "0xcc926f018f6f1052987e732c8250bbb7a73d83a3",
    "WMO-1":    "0x3d0c46847b3a6ad1eb1f7409c83acd63f60454db",
    "CITY-1":   "0x2a62d902b235f8557d97c4eaefffcb5bf14be75a",
    "REC-2":    "0x6feb1d0347fbb1c3c9cb574380b5ba93666cf6f3",
    "WMO-2":    "0x936468a25face673e4ae6bd928fb74611ae9b959",
    "CITY-2":   "0x7c9849442b0c30ec530ee4957f4600abe913c962",
}

// struct to model data that gets stored in the SQlite3 backend db for statistics
type Balance struct {
	Address string
	EthBalance int64
	Timestamp int64
}

// struct metho to conveniently save() Balance data
func (b *Balance) saveToDB() error {
	stmt, err := db.Prepare("INSERT INTO balances(address, ethbalance, timestamp) values(?, ?, ?);")
    _, err = stmt.Exec(b.Address, b.EthBalance, b.Timestamp)
    if err != nil {
        return err
    }
    return nil
}

// function to retrieve the balance of an account specified by public key string
func getBalanceInETH(account string) (int64, error) {
	// returned in Wei, needs conversion
	weiBalance, err := client.BalanceAt(context.Background(), common.HexToAddress(account), nil)
	if err != nil {
	    log.Println("Error: ", Red(err))
	    return 0, err
	}
	// convert WEI to ETH then return it
	floatWeiBalance := new(big.Float)
	floatWeiBalance.SetString(weiBalance.String())
	floatEthBalance := new(big.Float).Quo(floatWeiBalance, big.NewFloat(math.Pow10(18)))
	ethBalInt, _ := floatEthBalance.Int64()
	return ethBalInt, nil
}

// this function queries the local SQLite DB for new balance history and returns it as int slice
func getDataPoints(accnt string) []int {
	queryStr := fmt.Sprintf("SELECT ethbalance FROM balances WHERE address='%v';", accnt)
    rows, _ := db.Query(queryStr)
    var results []int
    var res int
    for rows.Next() {
        rows.Scan(&res)
        results = append(results, res)
    }
	if len(results) > 100 {
		return results[len(results)-100:]
	}
    return results
}

// this function executes in parallel go routine every 10 seconds and 
// saves current balances in PNG format to current directory
func plotAccountBalances(wmoaddr string, recaddr string) {

	// set some plotter config params
	dimensions, persist, debug := 2, true, false

	// instantiate a plot and use it persistently
	wmoPlot, _ := glot.NewPlot(dimensions, persist, debug)
	wmoPlot.SetTitle("Balance sheet (" +wmoaddr[:10]+"--"+wmoaddr[35:]+")")
	wmoPlot.Cmd("set terminal png size 1280,768")

	// instantiate another plot and use it persistently
	recPlot, _ := glot.NewPlot(dimensions, persist, debug)
	recPlot.SetTitle("Balance sheet (" +recaddr[:10]+"--"+recaddr[35:]+")")
	recPlot.Cmd("set terminal png size 1280,768")

	// execute every 10 seconds: get new PLOT data and SAVE it	
	for {
		wmoPlot.AddPointGroup("WMO-1", "lines", getDataPoints(wmoaddr))
		wmoPlot.SavePlot("WMO-1.png")
		recPlot.AddPointGroup("REC-1", "lines", getDataPoints(recaddr))
		recPlot.SavePlot("REC-1.png")
		time.Sleep(10*time.Second)
	}
}

func main() {
	// check if DB file exists, if not create it in current working directory
	_, err := os.Stat(DB_FILE)
    if os.IsNotExist(err) {
        log.Printf("Database File %s exist! Let's create one locally...", Red("DOES NOT"))
        tmpDBHandler, _ := sql.Open("sqlite3", DB_FILE)
        _, err := tmpDBHandler.Exec(DB_SCHEMA)
        if err != nil {
            log.Println(err)
        }
        tmpDBHandler.Close()
    }

    // open DB connection and store reference to handler in global variable
    log.Printf("Opening database file ...\n")
    db, _ = sql.Open("sqlite3", DB_FILE)
    defer db.Close()

    // open ETH RPC connection
	log.Println("Dialing in from the", Yellow("ETH RPC"), "Client...")
	client, err = ethclient.Dial("http://" + "10.0.0.51:8080")
    if err != nil {
        log.Fatal(err)
    }

    // sleep for 5 sec to let other nodes start up as well
    log.Println("Starting operator in", Yellow("5 sec"), "...")
	time.Sleep(5*time.Second)

	// start parallel go-routine to save PLOT in specified location 
    go plotAccountBalances(ADDRESS_BOOK["WMO-1"], ADDRESS_BOOK["REC-1"])

	// implement main logic in infinite loop:
	//	"get account balance for each interesting account every 15 sec
	// 	and store it in SQlite db to be able to show historical"
    for {

    	// loop through accounts of interest
    	for _, accnt := range []string{ADDRESS_BOOK["WMO-1"], ADDRESS_BOOK["REC-1"]} {
    		
    		// fetch balance via ETH RPC
    		balance, err := getBalanceInETH(accnt)
    		log.Println("Balance: ", Green(balance), " Account: ", Green(accnt))
    		if err != nil {
    			log.Println("Error: ", Red(err))
    			continue
    		}
    		// construct Balance data struct
	    	blnc := Balance{
		    	Address: accnt,
		    	EthBalance: balance,
		    	Timestamp: time.Now().Unix(),
		    }
	    	// try to save it to the local db
		    err = blnc.saveToDB()
		    if err != nil {
		    	log.Printf("Err: %+v\n", Red(err))
		    }	
    	}
    	// wait for 15 sec before next iteration
	    time.Sleep(15*time.Second)
	}    
}