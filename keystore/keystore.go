package main

import (
    "fmt"
    "os"
    "net/http"
    "encoding/hex"
    "encoding/json"
    "io/ioutil"
    "log"
    "strings"
    "strconv"
    "time"
    "database/sql"
    _ "github.com/mattn/go-sqlite3"
    "crypto/sha256"
)

var PORT = "55554"

const (
    // global DB file name
    DB_FILE = "storage.db"

    DB_SCHEMA = `CREATE TABLE keystore (
                    id INTEGER PRIMARY KEY AUTOINCREMENT,
                    channel TEXT NOT NULL,
                    key BLOB NOT NULL UNIQUE,
                    sha TEXT NOT NULL UNIQUE,
                    timestamp INTEGER NOT NULL
                );`

    // optional landing page for developer informatio
    LANDING_PAGE= `
    <html>
      <head>
        <title> Keyworker REST API landing page</title>
      </head>
      <body>
        <h1> Keyworker REST API: </h1>
        <ul>
          <li><a href="/upload">"/upload"</a> -> Upload key material (HTTP POST-only).</li>
          <li><a href="/getkeybysha">"/getkeybysha"</a> -> GET Key By Hex-encoded SHA value.</li>
          <li><a href="/getkey">"/getkey"</a> -> GET new batch of keys by supplying channel ID.</li>
        </ul>
      </body>
    </html>`
)

// global variable for DB
var db *sql.DB

// Struct to handle data for Keys from the QKD Emulator
type KeyData struct {
    Channel     string `json:"channel"`   // example: SGB1, SGB2, SGB3, etc...
    Key         []byte `json:"key"`       // encoded as base64 when sent in JSON format
    Sha         string `json:"sha"`       // HEX string representation of the byte[]
    Timestamp   int64  `json:"timestamp"` // time of creation of key
}

// struct method that prints the fields of its struct
func (k *KeyData) print() {
    fmt.Printf("Channel: %s\nKey: %v\nSha: %v\nTimestamp: %v\n",
        k.Channel,
        k.Key,
        k.Sha,
        time.Unix(k.Timestamp, 0).UTC())
}

// struct method to check if key is empty or not
func (k *KeyData) isEmpty() bool {
    if ( (k.Channel == "") || (len(k.Key) == 0) || (k.Sha == "") || (k.Timestamp == 0) ) {
        return true
    }
    return false
}

// saves a KeyData struct to the SQLITE database that was opened at startup
func (k *KeyData) saveToDB() error {

    // CHECK QUICKLY IF CHANNEL EXISTS ALREADY (we only store latest key for each)
    var channel_exists int
    err := db.QueryRow("select count() from keystore WHERE channel=?;", k.Channel).Scan(&channel_exists)
    if err != nil {
        log.Fatal(err)
    }
    // if it does exist then SQL UPDATE
    if channel_exists > 0 {
        // log.Println("UPDATE existing key record...")

        stmt, err := db.Prepare("UPDATE keystore SET key=?, sha=?, timestamp=? WHERE channel = ?;")
        _, err = stmt.Exec(k.Key, k.Sha, k.Timestamp, k.Channel)
        if err != nil {
            return err
        }
        return nil
    // otherwise SQL INSERT
    } else {
        // log.Println("INSERT new key record...")

        stmt, err := db.Prepare("INSERT INTO keystore(channel, key, sha, timestamp) values(?, ?, ?, ?);")
        _, err = stmt.Exec(k.Channel, k.Key, k.Sha, k.Timestamp)
        if err != nil {
            return err
        }
        return nil
    }
}

// Struct to handle and authenticate requests for new keys!
type ChannelKeyRequest struct {
    Channel string `json:"channel"`  // example: 100-101, 100-102, 100-103
}

// Below code can be used to insert into a handler function and will
// print out debug info about incoming request headers and body
// ===========================================================
//  dump := func(r *http.Request) {
//      output, err := httputil.DumpRequest(r, true)
//      if err != nil {
//          fmt.Println("Error dumping request:", err)
//          return
//      }
//      fmt.Println(string(output))
//  }

//  dump(r)
// ===========================================================

// Helper function to write a message with code in JSON format
func ReplyWithJSON(w http.ResponseWriter, msg interface{}, code int) {
    log.Printf("Outgoing HTTP Response Code: %d\n", code)
    jsonResp, _ := json.MarshalIndent(msg, "", " ")
    w.Header().Set("Content-Type", "application/json")
    w.WriteHeader(code)
    w.Write(jsonResp)
}

// Handler function for /upload to POST new key material and store it in DB
func UploadKeyHandler(w http.ResponseWriter, r *http.Request) {
    // basic logging of the incoming request
    log.Println("Incoming HTTP " + string(r.Method) + " to \""+r.URL.Path+"\" from " + string(r.RemoteAddr))

    // limit to POST only and enforce json encoding
    if r.Method != "POST" {
        w.Header().Add("Access-Control-Allow-Methods", "POST")
        ReplyWithJSON(w, map[string]string{"error": "Only HTTP POST is supported!"}, http.StatusMethodNotAllowed)
        return
    }
    // enforce JSON encoding on incoming requests
    if r.Header.Get("Content-type") != "application/json" {
        ReplyWithJSON(w, map[string]string{"error": "Payload required in JSON format!"}, http.StatusUnsupportedMediaType)
        return
    }

    // Read body of the request into byte array [b]
    b, err := ioutil.ReadAll(r.Body)
    defer r.Body.Close()
    if err != nil {
        ReplyWithJSON(w, map[string]string{"error": err.Error()}, 500)
        return
    }

    // unMarshal byte array body from JSON into KeyData struct
    var newKey KeyData
    err = json.Unmarshal(b, &newKey)
    if err != nil {
        ReplyWithJSON(w, map[string]string{"error": err.Error()}, 500)
        return
    }

    // validate if two fields were submitted correctly
    if ( (len(newKey.Key) != 32) || (newKey.Channel == "") ) {
        ReplyWithJSON(w, map[string]string{"error": "Invalid Data Submitted in JSON request"}, http.StatusBadRequest)
        return
    }

    // fill in the KeyData with current timestamp
    newKey.Timestamp = time.Now().Unix()

    // calculate SHA256 of Key encoded as HEX
    h := sha256.New()
    h.Write(newKey.Key)
    newKey.Sha = hex.EncodeToString(h.Sum(nil))

    // store the processed key in the DB, check if any errors occured
    if err := newKey.saveToDB(); err != nil {
        if strings.Contains(err.Error(), "UNIQUE constraint failed") {
            ReplyWithJSON(w, map[string]string{"error": "Key conflict (NOT UNIQUE)"}, http.StatusConflict)
            return
        }
        ReplyWithJSON(w, map[string]string{"error": "Unable to process request"}, http.StatusBadRequest)
        return
    }

    // return success message in case no errors were found
    ReplyWithJSON(w, map[string]string{"result": "Key Upload Succesful!"}, http.StatusCreated)
}

//helper function to get KeyData from SQLite3 database
func dbGetKeyForChannel(channel string) KeyData {
    // empty slice to hold the individual keys
    var key KeyData
    err := db.QueryRow(`SELECT channel, key, sha, timestamp 
                        FROM keystore WHERE channel=?;`, channel).Scan(&key.Channel,
                                                                       &key.Key,
                                                                       &key.Sha,
                                                                       &key.Timestamp)
    if err != nil {
        // log.Println(err)
        return KeyData{}
    }

    return key
}

// TODO: implement handler for "/getkey" API endpoint
func GetChannelKeyHandler(w http.ResponseWriter, r *http.Request) {
    log.Println("Incoming HTTP " + string(r.Method) + " to \""+r.URL.Path+"\" from " + string(r.RemoteAddr))

    // enforce GET-only on this API endpoint
    if r.Method != "GET" {
        w.Header().Add("Access-Control-Allow-Methods", "GET")
        ReplyWithJSON(w, map[string]string{"error": "Only HTTP GET is supported!"}, http.StatusMethodNotAllowed)
        return
    }

    // enforce JSON encoding on incoming requests
    if r.Header.Get("Content-type") != "application/json" {
        ReplyWithJSON(w, map[string]string{"error": "Payload required in JSON format!"}, http.StatusUnsupportedMediaType)
        return
    }

    // Read body of the request into byte array [b]
    b, err := ioutil.ReadAll(r.Body)
    defer r.Body.Close()
    if err != nil {
        ReplyWithJSON(w, map[string]string{"error": err.Error()}, 500)
        return
    }

    // unMarshal byte array body from JSON into KeyData struct
    var keyreq ChannelKeyRequest
    err = json.Unmarshal(b, &keyreq)
    if err != nil {
        ReplyWithJSON(w, map[string]string{"error": err.Error()}, 500)
        return
    }

    // get key and check if anything valid was retrieved
    channelKey := dbGetKeyForChannel(keyreq.Channel)
    if channelKey.isEmpty() {
        // log.Println("ERROR: Not found!")
        ReplyWithJSON(w, map[string]string{"error": "Not found!"}, http.StatusNotFound)
        return
    }

    // render key as JSON response
    ReplyWithJSON(w, channelKey, http.StatusOK)
}

// helper function to search the DB for a given HASH and return a KeyData if found
func dbSearchKeyBySHA(sha string) KeyData {
    queryStr := fmt.Sprintf("SELECT channel, key, sha, timestamp FROM keystore WHERE sha = '%v';", sha)
    rows, _ := db.Query(queryStr)
    var key KeyData
    for rows.Next() {
        rows.Scan(&key.Channel, &key.Key, &key.Sha, &key.Timestamp)
    }
    return key
}

// handler function for "/getkeybysha" API endpoint
func GetKeyByShaHandler(w http.ResponseWriter, r *http.Request) {
    log.Println("Incoming HTTP " + string(r.Method) + " to \""+r.URL.Path+"\" from " + string(r.RemoteAddr))

    // enforce HTTP GET on the incoming requests
    if r.Method != "GET" {
        w.Header().Add("Access-Control-Allow-Methods", "GET")
        ReplyWithJSON(w, map[string]string{"error": "Only HTTP GET is supported!"}, http.StatusMethodNotAllowed)
        return
    }
    // enforce JSON encoding on incoming requests
    if r.Header.Get("Content-type") != "application/json" {
        ReplyWithJSON(w, map[string]string{"error": "Payload required in JSON format!"}, http.StatusUnsupportedMediaType)
        return
    }

    // Read body into byte array [b]
    b, err := ioutil.ReadAll(r.Body)
    defer r.Body.Close()
    if err != nil {
        ReplyWithJSON(w, map[string]string{"error": err.Error()}, 500)
        return
    }

    // unMarshal JSON into KeyData struct
    var kd KeyData
    err = json.Unmarshal(b, &kd)
    if err != nil {
        log.Println(err)
        ReplyWithJSON(w, map[string]string{"error": err.Error()}, 500)
        return
    }

    // search for the key in DB by the SHA from the request
    key := dbSearchKeyBySHA(kd.Sha)
    // if Owner is empty it means nothing was found,
    if key.Channel == "" {
        ReplyWithJSON(w, map[string]string{"error": "Key not found in DB!"}, http.StatusNotFound)
        return
    }

    // otherwise the key is found and can be returned in JSON format
    ReplyWithJSON(w, key, http.StatusOK)
}

// implement a landing page with explanation on available API endpoints
func defaultHandler(w http.ResponseWriter, r *http.Request) {
    log.Println("Incoming HTTP " + string(r.Method) + " to \""+r.URL.Path+"\" from " + string(r.RemoteAddr))
    w.WriteHeader(200)
    w.Write([]byte(LANDING_PAGE))
}

func main() {

    // check for optional CLI argument for using non-default port
    if len(os.Args[1:]) != 0 {
        _, err := strconv.Atoi(os.Args[1])
        if err != nil {
            log.Println("ERROR: Invalid CLI argument!\nUSAGE: ./keystore [PORT-optional-integer]")
            os.Exit(1)
        }
        PORT = os.Args[1]
    }

    // check if DB file exists, if not create it in local directory
    if _, err := os.Stat(DB_FILE); os.IsNotExist(err)  {
        // path/to/whatever does *not* exist
        log.Println("Database File does NOT exist! Let's create one locally...")
        tmpDBHandler, _ := sql.Open("sqlite3", DB_FILE)
        _, err := tmpDBHandler.Exec(DB_SCHEMA)
        if err != nil {
            log.Println(err)
        }
        tmpDBHandler.Close()
    }

    // open DB and store a reference in global variable 'db'
    log.Printf("Opening database file from disk...\n")
    db, _ = sql.Open("sqlite3", DB_FILE)
    defer db.Close()

    // print log to announce running of server
    log.Printf("Keystore REST API running on port: %v\n", PORT)

    // HTTP REST Handlers definition
    http.HandleFunc("/upload", UploadKeyHandler)
    http.HandleFunc("/getkey", GetChannelKeyHandler)
    http.HandleFunc("/getkeybysha", GetKeyByShaHandler)
    // http.HandleFunc("/identities", IdentitiesHandler)
    http.HandleFunc("/", defaultHandler)
    
    // start HTTP REST API listener
    log.Fatal(http.ListenAndServe(":"+PORT, nil))
}
