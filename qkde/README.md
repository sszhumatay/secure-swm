# QKDE implementation details

This program is a modified version of qkde from BAL repository.
It tries to emulate a Quantum Key Distribution channel.
It was written to interact with the KEYSTORE program and its main functionality is to generate random symmetric encryption keys and submit them for storing to the KEYSTORE. 
It operates by sending HTTP POST requests to the `/upload` API endpoint of a KEYSTORE instance on a periodic basic.