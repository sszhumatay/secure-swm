# ETH_GW implementation details

This program is written for Scenario V3 to interact with the private Ethereum network via RPC calls carried over JSON.

Calling the program:
```
./ethgw -mode=[sync/non-sync] -eth=IP:PORT -qnet=IP:PORT
```
- `eth=IP:PORT` CLI flag lets the caller configure the ETH RPC endpoint
- `qnet=IP:PORT` CLI flag lets the caller configure the Quant BCNode endpoint.
- `mode=SYNC` cli is to make it function as GW1 on the topology, it will act as a glue between QNet BC and Ethereum BC. 
- `mode=NON-SYNC` is to make it function as GW2 on the topology, it will accept SGB TX-es and submit them to Ethereum and Qnet BCs.

## SYNC Mode:

The program will periodically query the Qnet BCNode endpoint and get the latest blockchain in JSON format.
Because the QNet BCnodes lack an automatic trigger for the consensus, this node will tkae care of that by calling the necessary API endpoing on Quant BCNode `/nodes/resolve` to trigger it every 10 iterations.
Once the blockchain is obtained it will be parsed and the GW node will iterate through each block and submit all valid TX-es in parallel to the Ethereum RPC and will wait until all are confirmed before moving on to the next block.


## NON-SYNC Mode:

The program will listen on port 8888 (or else if specified) and accept SGB transactions.
These SGB TX-es will first be validated and then sent to Ethereum RPC (Eth Priv Network #2).
Once the TX-es sent to the Ethereum private network are confirmed the GW will also submit them to the Quant BCNode so it can be propagated to the Ethereum Private Network #1.