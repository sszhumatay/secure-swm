from mininet.moduledeps import pathCheck
from mininet.node import CPULimitedHost
from mininet.log import debug

## TODO: Check these imports
import os
import requests

from subprocess import call, check_output

from multiprocessing import Process
from itertools import combinations

from mininet.util import quietRun
from mininet.moduledeps import pathCheck
from mininet.cli import CLI
from mininet.node import Host, CPULimitedHost
from mininet.log import info, error, warn, debug
##


class SGBClientNode(CPULimitedHost):
    """An SGBClientNode is a simple python app running on SGB Nodes and submit transactions to Server Nodes"""

    def __init__( self, name, inNamespace=True, client='sgb_emu.py',
                  cdir=None, ip="127.0.0.1",  **params ):
        self.name = name
        self.cdir = cdir
        self.ip = ip
        self.cout = "/var/tmp/" + self.name + '.log'

        # below is needed to eliminate hardcoded path to sgb_emu.py
        dir_path = os.path.dirname(os.path.realpath(__file__))
        self.client = dir_path + '/' + client

        CPULimitedHost.__init__( self, name, inNamespace=inNamespace,
                       ip=ip, **params  )

    def change_cout_dir_to(self, newcout):
        self.cout = newcout + self.name + '.log'

    # remove default value for target_address?
    def start( self, target_address='10.0.0.100', is_ether_scenario=False):
        if self.client:
            pathCheck( self.client )
            if self.cdir is not None:
                self.cmd( 'cd ' + self.cdir )
            if is_ether_scenario:
                cmd = self.client + ' ' + self.name +' '+ target_address + " eth"
            else:
                cmd = self.client + ' ' + self.name +' '+ target_address 
            debug( cmd + ' 1>' + self.cout + ' 2>' + self.cout + ' &' )
            self.cmd( cmd + ' 1>' + self.cout + ' 2>' + self.cout + ' &' )
            self.execed = False

    def stop( self, *args, **kwargs ):
        """Stop node."""
        self.cmd( 'kill %' + self.client )
        self.cmd( 'wait %' + self.client )
        # no need to call this, net.stop() will take care of it for us
        #super( SGBClientNode, self ).stop( *args, **kwargs )

class QKDEmuNode(CPULimitedHost):
    """An QKDEmuNode is a Mininet node running multiple instances of QKDEMU app """

    def __init__( self, name, inNamespace=True, client='qkdego',
                  ip="127.0.0.1",  **params ):
        self.name = name
        self.ip = ip
        self.client = client
        self.cout = '/var/tmp/' + self.name + '.log'

        # below is needed to eliminate hardcoded path to executable
        # dir_path = os.path.dirname(os.path.realpath(__file__))
        # self.client = dir_path + '/' + client

        CPULimitedHost.__init__( self, name, inNamespace=inNamespace,
                       ip=ip, **params  )

    def change_cout_dir_to(self, newcout, instance):
        self.cout = newcout + self.name + "_instance_" + instance + '.log'

    def start_qkd_emulators(self, kw_list):
        """
        Takes the job of creating KW pairs and starting new instance of QKDE for each pair
        """
        instances = ['100-101', '100-102', '100-103', '101-102', '101-103', '102-103']
        kw_pairs = [i for i in combinations(kw_list, 2)]
        i = 0
        for kw_pair in kw_pairs:
            self.start(instances[i], list(kw_pair))
            i+=1
 
    def start( self, instance, keyworkers_list):
        """
        Starts a new instance of QKDE with given parameters
        """
        if self.client:
            pathCheck( self.client )
            # cout = '/var/tmp/' + self.name + "_instance_" + instance + '.log'
            self.cout = "/var/tmp/eth-scenario/logs/" + self.name + "_instance_" + instance + '.log'

            cmd = self.client +' '+ ' '.join(keyworkers_list) 
            debug( cmd + ' 1> ' + self.cout + ' 2> ' + self.cout + ' & ' )

            self.cmd( cmd + ' 1> ' + self.cout + ' 2> ' + self.cout + ' & ' )
            self.execed = False

    def stop( self, *args, **kwargs ):
        """Stop node."""
        self.cmd( 'kill %' + self.client )
        self.cmd( 'wait %' + self.client )

class GWNode(CPULimitedHost):
    """An GWNode is a simple node running taking care of requests coming in from SGBs and Trucks and s"""
    def __init__( self, name, inNamespace=True, client='ethgw',
                  cdir=None, ip="127.0.0.1",  **params ):
        self.name = name
        self.cdir = cdir
        self.ip = ip
        self.cout = '/var/tmp/eth-scenario/logs/ethgw_' + self.name + '.log'
        self.client = client

        CPULimitedHost.__init__( self, name, inNamespace=inNamespace,
                       ip=ip, **params  )

    # remove default value for eth_target?
    def start( self, mode, eth_target, qnet_target):
        if self.client:
            pathCheck( self.client )
            if self.cdir is not None:
                self.cmd( 'cd ' + self.cdir )
            cmd = self.client +' -mode=' +mode + ' -eth='+ eth_target + " -qnet=" + qnet_target
            debug( cmd + ' 1>' + self.cout + ' 2>' + self.cout + ' &' )
            self.cmd( cmd + ' 1>' + self.cout + ' 2>' + self.cout + ' &' )
            self.execed = False

    def stop( self, *args, **kwargs ):
        """Stop node."""
        self.cmd( 'kill %' + self.client )
        self.cmd( 'wait %' + self.client )
        # no need to call this, net.stop() will take care of it for us
        #super( SGBClientNode, self ).stop( *args, **kwargs )

class TruckNode(CPULimitedHost):
    """A TruckNode is a simple node emulating a truck that collects waste and distributes Quantum keys"""
    def __init__( self, name, inNamespace=True, client='truck_node.py',
                  cdir=None, ip="127.0.0.1",  **params ):
        self.name = name
        self.cdir = cdir
        self.ip = ip
        self.cout = '/var/tmp/eth-scenario/logs/truck_' + self.name + '.log'

        # below is needed to eliminate hardcoded path to sgb_emu.py
        dir_path = os.path.dirname(os.path.realpath(__file__))
        self.client = dir_path + '/' + client

        CPULimitedHost.__init__( self, name, inNamespace=inNamespace,
                       ip=ip, **params  )

    # remove default value for target_address?
    def start( self, target_address='10.0.0.100'):
        if self.client:
            pathCheck( self.client )
            if self.cdir is not None:
                self.cmd( 'cd ' + self.cdir )
            cmd = self.client + ' ' + self.name +' '+ target_address 
            debug( cmd + ' 1>' + self.cout + ' 2>' + self.cout + ' &' )
            self.cmd( cmd + ' 1>' + self.cout + ' 2>' + self.cout + ' &' )
            self.execed = False

    def stop( self, *args, **kwargs ):
        """Stop node."""
        self.cmd( 'kill %' + self.client )
        self.cmd( 'wait %' + self.client )
        # no need to call this, net.stop() will take care of it for us
        #super( SGBClientNode, self ).stop( *args, **kwargs )


class OperatorsNode(CPULimitedHost):
    """An GWNode is a simple node running taking care of requests coming in from SGBs and Trucks and s"""
    def __init__( self, name, inNamespace=True, client='operators',
                  cdir="/var/tmp/op_db", ip="127.0.0.1",  **params ):
        self.name = name
        self.cdir = cdir
        self.ip = ip
        self.cout = '/var/tmp/eth-scenario/logs/opnode_' + self.name + '.log'
        self.client = client

        CPULimitedHost.__init__( self, name, inNamespace=inNamespace,
                       ip=ip, **params  )

    # remove default value for target_address?
    def start( self ):
        if self.client:
            pathCheck( self.client )
            if self.cdir is not None:
                self.cmd( 'cd ' + self.cdir )
            cmd = self.client
            debug( cmd + ' 1>' + self.cout + ' 2>' + self.cout + ' &' )
            self.cmd( cmd + ' 1>' + self.cout + ' 2>' + self.cout + ' &' )
            self.execed = False

    def stop( self, *args, **kwargs ):
        """Stop node."""
        self.cmd( 'kill %' + self.client )
        self.cmd( 'wait %' + self.client )


class BCNode( CPULimitedHost):
    """A BCNode is a Node that is running (or has execed?) an
       block[chain] application."""

    def __init__( self, name, inNamespace=True,
                  server='', sargs='', sdir='/var/tmp',
                  client='', cargs='{command}', cdir=None,
                  ip="127.0.0.1", port='', **params ):
        # Server params
        self.server = server
        self.sargs = sargs
        self.sdir = sdir
        
        # Client params
        self.client = client
        self.cargs = cargs
        self.cdir = cdir

        self.ip = ip
        self.port = port
        # for passing consistent UUID to blockchain nodes
        self.id_store={
                "srv1" : "60c42574f5ca4f3b9057207b7f332a68",
                "srv2" : "076584247dc041ad9182f58255b31c09",
                "srv3" : "a655c3ab2e6e48d49d18ae34ba6bcc25",
                "srv4" : "52ea9e5e4fd111e99eb9734ce850821e"
        }
        CPULimitedHost.__init__( self, name, inNamespace=inNamespace, ip=ip, **params  )

    def start( self ):
        """Start <bcnode> <args> on node.
           Log to /tmp/bc_<name>.log"""
        if self.server:
            pathCheck( self.server )
            cout = '/var/tmp/bc_' + self.name + '.log'
            if self.sdir is not None:
                self.cmd( 'cd ' + self.sdir )
            cmd = self.server
            if self.sargs:
                cmd += " " + self.sargs.format(name=self.name,
                                               node_id=self.id_store[self.name], ## for format into SARGS
                                               IP=self.IP(),
                                               port=self.port,
                                               cdir=self.cdir,
                                               sdir=self.sdir)
            debug( cmd + ' 1>' + cout + ' 2>' + cout + ' &' )
            self.cmd( cmd + ' 1>' + cout + ' 2>' + cout + ' &' )
            self.execed = False

    def stop( self, *args, **kwargs ):
        "Stop node."
        self.cmd( 'kill %' + self.server )
        self.cmd( 'wait %' + self.server )
        # > No need to call this, net.stop() will do it for us..
        # super( BCNode, self ).stop( *args, **kwargs )

    def isAvailable( self ):
        "Is executables available?"
        cmd = 'which '
        if self.server:
            cmd += self.server + ' '
        if self.client:
            cmd += self.client
        return quietRun(cmd)


    def call(self, command, data=''):
        """Call <client> <cargs> on node."""
        if self.cdir is not None:
            self.cmd( 'cd ' + self.cdir )
        cmd = self.client
        pathCheck( cmd )

        if self.cargs:
            cmd += " " + self.cargs.format(command=command,
                                           data=data,
                                           name=self.name,
                                           IP=self.IP(),
                                           port=self.port,
                                           cdir=self.cdir,
                                           sdir=self.sdir)
            if data:
                cmd += " " + self.cargs.format(data=data)
        else:
            cmd += " "  + command
        #print (cmd)
        result = self.cmdPrint( cmd )

        debug("command: %s = %s" % (cmd, result))
        return result

class POWNode(BCNode):
    """A POWNode is a BCNode that is running an POWBlockChain."""

    def __init__( self, name, inNamespace=True,
                  server='bcn.py',
                  sargs='-p {port} -d {sdir}/pow-{IP}.db -v pow:5 -n {node_id}', # ADDED -n (node_ID)
                  sdir='/var/tmp',
                  client='curl',
                  cargs="-s -X GET -H 'Content-Type: application/json' -d '{data}' http://{IP}:{port}/{command}",
                  cdir=None,
                  ip="127.0.0.1", port='5000', **params ):
        # needed to eliminate full-path that contained my file-system and made it impossible to be portable
        dir_path = os.path.dirname(os.path.realpath(__file__))
        srv_name = server
        BCNode.__init__( self, name, inNamespace=inNamespace,
                         server=dir_path+'/'+srv_name, sargs=sargs, sdir=sdir,
                         client=client, cargs=cargs, cdir=cdir,
                         ip=ip, port=port, **params )

class QNode(BCNode):
    """A QNode is a BCNode that is running an QuantumBlockChain application."""

    def __init__( self, name, inNamespace=True,
                  server='bcn.py',
                  sargs='-p {port} -d {sdir}/qkd-{IP}.db -v quant -n {node_id}',
                  sdir='/var/tmp',
                  client='curl',
                  cargs="-s -X GET -H 'Content-Type: application/json' -d '{data}' http://{IP}:{port}/{command}",
                  cdir=None,
                  keyworker='keystore', kargs='55554', kdir='/var/tmp',
                  qkdemu='qkdemu', eargs='http://localhost:55554', edir=None,
                  ip="127.0.0.1", port='5000', **params ):

        # Keyworker params
        self.keyworker = keyworker
        self.kargs = kargs
        if name == 'srv1':
            self.kdir = kdir+"/kw1_db"
        elif name == 'srv2':
            self.kdir = kdir+"/kw2_db"
        elif name == 'srv3':
            self.kdir = kdir+"/kw3_db"
        elif name == 'srv4':
            self.kdir = kdir+"/kw4_db"

        # QKD emulator params
        self.qkdemu = qkdemu
        self.eargs = eargs
        self.edir = edir

        self.name = name

        self.cout = '/var/tmp/kw_' + self.name + '.log'

        dir_path = os.path.dirname(os.path.realpath(__file__))
        srv_name = server

        BCNode.__init__( self, name, inNamespace=inNamespace,
                         server=dir_path+'/'+srv_name, sargs=sargs, sdir=sdir,
                         client=client, cargs=cargs, cdir=cdir,
                         ip=ip, port=port, **params )

    def kwstart( self ):
        """Start keyworker on node.
           Log to /tmp/kw_<name>.log"""
        if self.keyworker:
            pathCheck( self.keyworker )
            cout = '/var/tmp/kw_' + self.name + '.log'
            if self.kdir is not None:
                self.cmd( 'cd ' + self.kdir )
            debug( self.keyworker + ' ' + self.kargs +' 1>' + cout + ' 2>' + cout + ' &' )
            self.cmd( self.keyworker + ' ' + self.kargs +' 1>' + cout + ' 2>' + cout + ' &' )
            self.kexeced = False

    def kwstop( self, *args, **kwargs ):
        "Stop keyworker."
        self.cmd( 'kill %' + self.keyworker )
        self.cmd( 'wait %' + self.keyworker )
        #super( BCNode, self ).stop( *args, **kwargs )


class EthNode(CPULimitedHost):
    """A EthNode is a Mininet node that is running the Geth application."""

    def __init__( self, name, inNamespace=True, 
                  server='geth', sargs='--datadir bcdata/  --networkid 4321 --nodiscover', sdir='',
                  ip="127.0.0.1", **params ):
        
        self.name = name
        self.ip = check_output(['hostname', '-I']).strip('\n').strip(' ')
        self.server = server
        self.sargs = sargs
        self.sdir = sdir
        CPULimitedHost.__init__( self, name, inNamespace=inNamespace, ip=ip, **params)        
  
    def update_srv_working_dir(self, new_dir):
        self.sdir = new_dir

    def configure_main_node(self, ip):
        self.sargs += """ --rpc --rpcport "8080" --rpcaddr """ +ip+""" --rpccorsdomain "*" --rpcapi "db,eth,net,web3" --unlock '0, 1, 2, 3' --password 'pw.txt' --mine --minerthreads 1"""

    def start( self ):
        if self.server:
            pathCheck( self.server )
            cout = '/var/tmp/eth-scenario/logs/ethnode_' + self.name + '.log'
            if self.sdir is not None:
                self.cmd( 'cd ' + self.sdir )
            debug(    self.server + ' ' + self.sargs + ' 1>' + cout + ' 2>' + cout + ' &' )
            self.cmd( self.server + ' ' + self.sargs + ' 1>' + cout + ' 2>' + cout + ' &' )
            self.execed = False

    def stop( self, *args, **kwargs ):
        """Stop node."""
        self.cmd( 'kill %' + self.server )
        self.cmd( 'wait %' + self.server )

class BtcNode(BCNode):
    """A BtcNode is a BCNode that is running an Bitcoin application."""

    def __init__( self, name, inNamespace=True,
                  server='bitcoind', sargs='-regtest', sdir='/var/tmp',
                  client='bitcoin-cli',
                  cargs='-regtest -datadir={sdir}/{IP} {command}',
                  cdir=None,
                  ip="127.0.0.1", port='', **params ):

        BCNode.__init__( self, name, inNamespace=inNamespace,
                         server=server, sargs=sargs, sdir=sdir,
                         client=client, cargs=cargs, cdir=cdir,
                         ip=ip, port=port, **params )

    def start( self ):
        """Start <bcnode> <args> on node.
           Log to /tmp/bc_<name>.log"""
        if self.server:
            pathCheck( self.server )
            cout = '/var/tmp/bc_' + self.name + '.log'
            if self.sdir is not None:
                import os
                self.cmd( 'cd ' + self.sdir )
                sdir = '%s/%s' % (self.sdir, self.IP())
                try:
                    os.stat(sdir)
                except:
                    os.mkdir(sdir)
                self.sargs += ' -datadir=' + sdir
            debug(    self.server + ' ' + self.sargs + ' 1>' + cout + ' 2>' + cout + ' &' )
            self.cmd( self.server + ' ' + self.sargs + ' 1>' + cout + ' 2>' + cout + ' &' )
            self.execed = False
