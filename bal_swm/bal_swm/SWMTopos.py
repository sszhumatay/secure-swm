import time
import subprocess
import os
import atexit

from mininet.topo import Topo
from mininet.link import Link
from mininet.link import TCIntf
from mininet import util
from mininet.node import OVSSwitch
from mininet.log import info, error, warn, debug

# NEW
from swmnodes import   (SGBClientNode, 
                        POWNode, QNode, 
                        QKDEmuNode, EthNode, 
                        OperatorsNode, GWNode, 
                        TruckNode)

class SWMTopoPoW(Topo):
    "Custom Topo for SWM Scenario1 based on PoW Blockchain"

    def __init__(self):
        # Initialize topology
        Topo.__init__(self)

        info ("""\n\nTopology Build: \n

                                  
                                  Network: 10.0.0.X/24

                         .100                              .103
                      +------------+                +------------+
                      |            |                |            |
                      | SRV1 - PoW +------+  +------+ PoW - SRV4 |
                      |            |      |  |      |            |
                      +------------+      |  |      +------------+
                                          |  |
                      +------------+      |  |      +------------+
                      |            |      |  |      |            |
                      | SRV2 - PoW +----+ |  | +----+ PoW - SRV3 |
                      |            |    | |  | |    |            |
   +--------+         +------------+    | |  | |    +------------+         +-------+
.1 |   SGB1 +--+         .101           | |  | |           .102         +--+ SGB12 | .12
   +--------+  |                        | |  | |                        |  +-------+
               |  +--------+         +--+-+--+-+-+         +--------+   |
   +--------+  +--+        |         |           |         |        +---+  +-------+
.2 |   SGB2 +-----|   S1   +---------+    S5     +---------+  S4    |------+ SGB11 | .11
   +--------+  +--+        |         |           |         |        +---+  +-------+
               |  +--------+         +---+-----+-+         +--------+   |
   +--------+  |                         |     |                        |  +-------+
.3 |   SGB3 +--+                 +-------+     +-------+                +--+ SGB10 | .10
   +--------+                    |                     |                   +-------+
                            +----+---+            +----+---+
                            |        |            |        |
                            |  S2    |            |   S3   |
                            |        |            |        |
                            +-+-+--+-+            +--+-+-+-+
                              | |  |                 | | |
                         +----+ |  +---+        +----+ | +----+
                         |      |      |        |      |      |
                       +-+-+  +-+-+  +-+-+    +-+-+  +-+-+  +-+-+
                       | S |  | S |  | S |    | S |  | S |  | S |
                       | G |  | G |  | G |    | G |  | G |  | G |
                       | B |  | B |  | B |    | B |  | B |  | B |
                       | 4 |  | 5 |  | 6 |    | 7 |  | 8 |  | 9 |
                       +---+  +---+  +---+    +---+  +---+  +---+
                        .4     .5     .6       .7     .8     .9
\n\n""")

        # Add hosts and switches
        increment = 0
        switches = []
        hosts = []
        for i in range(1,6):
            switch = self.addSwitch('s'+str(i), cls = OVSSwitch, protocols='OpenFlow13')
            switches.append(switch)
            for j in range(1,5):
                if increment > 11:
                    host = self.addHost('srv'+str(j), ip='10.0.0.%s/24' % (increment+j+87), cls=POWNode)
                    hosts.append(host)
                    self.addLink(host, switch)
                else:
                    if j < 4:
                        host = self.addHost('sgb'+str(j+increment), ip='10.0.0.%s/24' % (j+increment), cls=SGBClientNode)
                        hosts.append(host)
                        self.addLink(host, switch)
            increment += 3

        for sw in switches[:-1]:
            self.addLink(sw, switches[-1]) 


class SWMTopoQuant(Topo):
    "Custom Topo for SWM Scenario1 based on Quantum Blockchain"

    def __init__(self):
        # Initialize topology
        Topo.__init__(self)

        info ("""\n\nTopology Build: \n
                                              .200
                                          +---------+
                                          |         |
      Network: 10.0.0.X/24                |   QKD   |
                                          |   EMU   |
                                          |         |
                             .100         ++---+---++          .103
                          +-------+----+   |   |   |    +----+-------+
                          |       | KW +---+   |   +----+ KW |       |
                          | SRV1  +----+   |   |   |    +----+  SRV4 |
                          |            +-----+ | +------+            |
                          +------------+   | | | | |    +------------+
                                           | | | | |
                          +-------+----+   | | | | |    +----+-------+
                          |       | KW +---+ | | | +----+ KW |       |
                          | SRV2  +----+     | | |      +----+  SRV3 |
                          |            +---+ | | | +----+            |
       +--------+         +------------+   | | | | |    +------------+         +-------+
    .1 |   SGB1 +--+         .101          | | | | |           .102         +--+ SGB12 | .12
       +--------+  |                       | | | | |                        |  +-------+
                   |  +--------+         +-+-+-+-+-+-+         +--------+   |
       +--------+  +--+        |         |           |         |        +---+  +-------+
    .2 |   SGB2 +-----|   S1   +---------+    S5     +---------+  S4    |------+ SGB11 | .11
       +--------+  +--+        |         |           |         |        +---+  +-------+
                   |  +--------+         +---+-----+-+         +--------+   |
       +--------+  |                         |     |                        |  +-------+
    .3 |   SGB3 +--+                 +-------+     +-------+                +--+ SGB10 | .10
       +--------+                    |                     |                   +-------+
                                +----+---+            +----+---+
                                |        |            |        |
                                |  S2    |            |   S3   |
                                |        |            |        |
                                +-+-+--+-+            +--+-+-+-+
                                  | |  |                 | | |
                             +----+ |  +---+        +----+ | +----+
                             |      |      |        |      |      |
                           +-+-+  +-+-+  +-+-+    +-+-+  +-+-+  +-+-+
                           | S |  | S |  | S |    | S |  | S |  | S |
                           | G |  | G |  | G |    | G |  | G |  | G |
                           | B |  | B |  | B |    | B |  | B |  | B |
                           | 4 |  | 5 |  | 6 |    | 7 |  | 8 |  | 9 |
                           +---+  +---+  +---+    +---+  +---+  +---+
                            .4     .5     .6       .7     .8     .9
                             \n\n""")

        # Add hosts and switches
        increment = 0
        switches = []
        hosts = []
        for i in range(1,6):
            switch = self.addSwitch('s'+str(i), cls=OVSSwitch, protocols='OpenFlow13')
            switches.append(switch)
            for j in range(1,5):
                if increment > 11:
                    host = self.addHost('srv'+str(j), ip='10.0.0.%s/24' % (increment+j+87), cls=QNode)
                    hosts.append(host)
                    self.addLink(host, switch)
                else:
                    if j < 4:
                        host = self.addHost('sgb'+str(j+increment), ip='10.0.0.%s/24' % (j+increment), cls=SGBClientNode)
                        hosts.append(host)
                        self.addLink(host, switch)
            increment += 3

        qkdeNode = self.addHost('qkde', ip='10.0.0.200/24', cls=QKDEmuNode)
        hosts.append(qkdeNode)
        self.addLink(qkdeNode, switches[-1])

        for sw in switches[:-1]:
            self.addLink(sw, switches[-1])  


class SWMTopoQuantumEthereum(Topo):
    "Custom Topo for Scenario3 based on Quantum Blockchain and Ethereum"

    def __init__(self):
        # Initialize topology
        Topo.__init__(self)

        # hosts and switches container
        switches = []
        hosts = []

        for i in range(1,6):
            switch = self.addSwitch('s'+str(i), cls=OVSSwitch, protocols='OpenFlow13')
            switches.append(switch)

            if i == 1:
                host = self.addHost("eth11", ip="10.0.0.51/24", cls=EthNode)
                hosts.append(host)
                self.addLink(host, switch)

                host = self.addHost("eth12", ip="10.0.0.52/24", cls=EthNode)
                hosts.append(host)
                self.addLink(host, switch)

                host = self.addHost("gw1", ip="10.0.0.59/24", cls=GWNode)
                hosts.append(host)
                self.addLink(host, switch)

                host = self.addHost("op", ip="10.0.0.250/24", cls=OperatorsNode)
                hosts.append(host)
                self.addLink(host, switch)

            elif i == 2:
                host = self.addHost("srv1", ip="10.0.0.100/24", cls=QNode)
                hosts.append(host)
                self.addLink(host, switch)

            elif i == 3:
                host = self.addHost("qkde1", ip="10.0.0.150/24", cls=QKDEmuNode)
                hosts.append(host)
                self.addLink(host, switch)

            elif i == 4:
                host = self.addHost("srv2", ip="10.0.0.101/24", cls=QNode)
                hosts.append(host)
                self.addLink(host, switch)

            else:
                host = self.addHost("eth21", ip="10.0.0.61/24", cls=EthNode)
                hosts.append(host)
                self.addLink(host, switch)

                host = self.addHost("eth22", ip="10.0.0.62/24", cls=EthNode)
                hosts.append(host)
                self.addLink(host, switch)

                host = self.addHost("gw2", ip="10.0.0.69/24", cls=GWNode)
                hosts.append(host)
                self.addLink(host, switch)

                host = self.addHost("sgb1", ip="10.0.0.1/24", cls=SGBClientNode)
                hosts.append(host)
                self.addLink(host, switch)

                host = self.addHost("sgb2", ip="10.0.0.2/24", cls=SGBClientNode)
                hosts.append(host)
                self.addLink(host, switch)

                host = self.addHost("sgb3", ip="10.0.0.3/24", cls=SGBClientNode)
                hosts.append(host)
                self.addLink(host, switch)

                host = self.addHost("qkde2", ip="10.0.0.151/24", cls=QKDEmuNode)
                hosts.append(host)
                self.addLink(host, switch)

                host = self.addHost("tkd", ip="10.0.0.200/24", cls=TruckNode)
                hosts.append(host)
                self.addLink(host, switch)

        self.addLink(switches[0], switches[1])
        self.addLink(switches[1], switches[2])
        self.addLink(switches[2], switches[3])
        self.addLink(switches[3], switches[4])

# process = subprocess.Popen(
#     ['keyworker', '-w', '2'""", '-d', '1'"""],
#     #['keyworker', '-n', '/opt/bal/var/db1', '-w', '2'""", '-d', '1'"""],
#     preexec_fn=os.setpgrp)


# def exit_handler():
#     process.kill()


# atexit.register(exit_handler)
topos = {
    'swmtopopow': (lambda: SWMTopoPoW()),
    'swmtopoquant': (lambda: SWMTopoQuant())
}