# Python 3 -> 2 compatibility
try:  # Python 3
    from urllib.parse import urlparse
except:
    from urlparse import urlparse

import hashlib
import json
import requests
import abc
import shelve
import os
import base64

from subprocess import check_output
import time
from flask import jsonify, request
from builtins import super
from uuid import uuid4

from merkletools import MerkleTools


class BaseBlockChain(object):
    def __init__(self, node_identifier):
        # to hold outstanding TX-es submitted for the next block to be mined
        self.current_transactions = []
        
        # needed to fix a bug that resulted in invonsistent hash-chain when TX was submitted during mining
        self.extra_transactions = []
        
        # holds the blockchain data structure
        self.chain = []

        # boolean variable to check if mining is currently in progress
        self.pow_mining_running = False
        
        # LIST of peers running the same blockchain services
        self.nodes = []
        
        # persistent DB for storage on disk (Berkeley DB)
        self.db = None
        
        # Generate a globally unique address for this node
        self.node_identifier = node_identifier

        # discover node's own IP automatically
        self.ip_address = self.discover_own_ip_address()
        
        # Create the genesis block
        self.current_transactions.append({
            'sender': node_identifier,
            'recipient': "Satoshi Nakamoto",
            'amount': "1",
            'tx_id': "genesis",
            'timestamp': int(time.time()),
        })
        
        # mine the genesis block that was appended
        self.new_block(previous_hash='1', proof="100")
        pass

    def save_db(self):
        """
        Opens the persistent DB stored on disk and saves the current chain
        """
        if self.db is not None:
            db = shelve.open(self.db)
            db['chain'] = json.dumps(self.chain)
            db.close()

    def init_db(self, dbfile):
        """
        Loads the DB from disk and reads it into memory as the chain
        """
        self.db = dbfile
        db = shelve.open(self.db)
        try:
            if db['chain']:
                self.chain = json.loads(db['chain'])
                if len(self.chain) > 0:
                    self.last_block = self.chain[len(self.chain) - 1]
        except:
            db.close()
            self.save_db()

    def register_node(self, address):
        """
        Add a new node to the list of nodes
        :param address: Address of node. Eg. 'http://192.168.0.5:5000'
           """
        parsed_url = urlparse(address)
        if parsed_url.netloc:
            self.nodes.append(parsed_url.netloc)
        elif parsed_url.path:
            # Accepts an URL without scheme like '192.168.0.5:5000'.
            self.nodes.append(parsed_url.path)
        else:
            raise ValueError('Invalid URL')

    def remove_nodes(self):
        """
        Clear nodes list.
        """
        self.nodes = []

    @abc.abstractmethod
    def resolve_conflicts(self):
        return

    def discover_own_ip_address(self):
        return check_output(['hostname', '--all-ip-addresses']).strip('\n').strip(' ')

    def new_block(self, proof, previous_hash):
        """
        Create a new Block in the Blockchain
        :param proof: The proof given by the Proof of Work algorithm
        :param previous_hash: Hash of previous Block
        :return: New Block
        """

        # prepare the Merkle Tree
        mt = MerkleTools(hash_type="sha256")
        mt.add_leaf([json.dumps(h) for h in sorted(self.current_transactions)], True)
        mt.make_tree();
        
        # if tree is ready get the root hash
        if mt.is_ready:
            block = {
                'index': len(self.chain) + 1,
                'timestamp': int(time.time()),
                'proof': proof,
                'previous_hash': previous_hash or self.hash(self.chain[-1]),
                'merkle_root': mt.get_merkle_root(),
                'transactions': self.current_transactions,
            }

        #  Append new block to to blockchain
        self.chain.append(block)

        # Reset the current list of transactions to start on a new block
        self.current_transactions = []
        return block

    def new_transaction(self, txid, sender, recipient, amount, timestamp):
        """
        Creates a new transaction to go into the next mined Block
        :param sender: Address of the Sender
        :param recipient: Address of the Recipient
        :param amount: Amount
        :return: The index of the Block that will hold this transaction
        """
        self.current_transactions.append({
            'tx_id': txid,
            'sender': sender,
            'recipient': recipient,
            'amount': amount,
            'timestamp': timestamp,
        })

        return self.last_block['index'] + 1

    @property
    def last_block(self):
        return self.chain[-1]

    @staticmethod
    def hash(block):
        """
        Creates a SHA-256 hash of a Block
        :param block: Block
        """

        # We must make sure that the Dictionary is Ordered, or we'll have inconsistent hashes
        block_string = json.dumps(block, sort_keys=True).encode()
        return hashlib.sha256(block_string).hexdigest()

    @abc.abstractmethod
    def proof_of(self, last_block):
        return

    @abc.abstractmethod
    def valid_block(self, last_proof, proof, last_hash):
        return

    @abc.abstractmethod
    def full_chain(self):
        return

    # REST API function for performing mining
    def mine(self):
        # boolean variable to check if node is mining or not
        # helps to avoid inconsistent hash-chain
        self.pow_mining_running = True

        # We run the proof of work algorithm to get the next proof...
        last_block = self.last_block
        proof = self.proof_of(last_block)

        # We must receive a reward for finding the proof.
        # The sender is "0" to signify that this node has mined a new coin.
        self.new_transaction(
            txid="mining-reward",
            sender="0",
            recipient=self.node_identifier,
            amount="1",
            timestamp=int(time.time())
        )

        # Forge the new Block by adding it to the chain
        previous_hash = self.hash(last_block)
        block = self.new_block(proof, previous_hash)

        # Prepare a response to be sent back in JSON format about new BLOCK 
        response = {
            'message': "New Block Forged",
            'index': block['index'],
            'proof': block['proof'],
            'previous_hash': block['previous_hash'],
            'transactions': block['transactions'],
            'merkle_root' : block['merkle_root']
        }

        # save_db() used to be before self.new_block() was called... not sure why...
        self.save_db()

        # reset the boolean variable that signals mining is currently running...
        self.pow_mining_running = False

        # return the new block forged in JSON response
        return jsonify(response), 200

    def get_nodes(self):
        """
        Function that returns the current registered neighbors
        """
        response = {
            'message': 'response to /nodes query',
            'total_nodes': list(self.nodes)
        }
        return jsonify(response)

    def register_nodes(self, values):
        """
        Function that parses incoming HTTPO POST and tries to register nodes if the info is valid
        """
        self.nodes = []

        # check if valid list of nodes was passed
        nodes = values.get('nodes')
        if nodes is None:
            return "Error: Please supply a valid list of nodes", 400

        # try to register each, catch any errors and exceptiosn rased
        try:
            for node in nodes:
                self.register_node(node)
        except ValueError as e:
            return jsonify({'error': str(e)}), 400

        # if no errors return success message
        response = {
            'message': 'New nodes have been added',
            'total_nodes': list(self.nodes),
        }
        return jsonify(response), 201

    def consensus(self):
        """
        Function that queries all registered neighbors and checks their chain length
        If a neighbor has a longer chain this node will adopt that whole chain as its own
        TODO: improve bandwidth > query only "length" param and select longest and fetch the chain
        """
        # call the resolution routine
        replaced = self.resolve_conflicts()

        # check if chain was modified and return msg accordingly
        if replaced:
            response = {
                'message': 'Consensus finished! Our chain was replaced'
            }
        else:
            response = {
                'message': 'Consensus finished! Our chain was kept as authoritative'
            }

        # return messages in JSON format
        return jsonify(response), 200

    def move_extras_to_main_tx_list(self):
        """
        Helper Function used to move pending transactions 
        to main pool once mining is finished
        """
        if len(self.extra_transactions) != 0:
            for tx in self.extra_transactions:
                self.current_transactions.append(tx)
            self.extra_transactions = []

    def valid_merkle_root(self, merkle_root, transactions):
        # check validity of merkle root hash
        mt = MerkleTools(hash_type="sha256")
        mt.add_leaf([json.dumps(h) for h in sorted(transactions)], True)
        mt.make_tree();
        
        # wait until all leafs are computed then compare
        if mt.is_ready:
            return mt.get_merkle_root() == merkle_root

    def forward_to_peers(self, tx_id, sender, recipient, amount, timestamp):
        data = {
                "status": "echoed",
                "tx_id": tx_id,
                "sender": sender,
                "recipient": recipient,
                "amount": amount,
                "timestamp": timestamp
        }
        for n in self.nodes:
            if type(n) == str:
                r = requests.post('http://{}/transactions/new'.format(n), json = data)
            elif type(n) == dict:
                r = requests.post('http://{}/transactions/new'.format(n["address"]), json = data)


class POWBlockChain(BaseBlockChain):
    def __init__(self, app, node_identifier, difficulty=4):
        super().__init__(node_identifier=node_identifier)
        
        # Proof of Work Difficulty setting
        self.difficulty = difficulty

        # reference to FLASK app to extend its endpoits
        self.app = app

        # function to auto-register other BCN nodes running in .100-120 IP address space
        self.discover_POW_nodes()

        # new API endpoint to handle HTTP POST to /chain (needed for mass resolve)
        @app.route('/chain', methods=['POST'])
        def full_chain_post():
            values = request.get_json()
            if values is None:
                return "Wrong params", 400
            if self.node_identifier not in values.get("path"):
                self.resolve_conflicts(path=values.get("path"))
            return jsonify(self.full_chain()), 201


    def is_chain_valid(self):
        """
        Determine if a our OWN blockchain is valid
        :param chain: A blockchain
        :return: True if valid, False if not
        """
        chain = self.chain
        last_block = chain[0]
        current_index = 1

        # loop through the whole blockchain from the first block after genesis block
        while current_index < len(chain):
            block = chain[current_index]
            
            if block['previous_hash'] != self.hash(last_block):
                return { 
                    "Error":"Invalid hash pointer",
                    "block_index":current_index,
                    "hash_block_header":block['previous_hash'],
                    "hash_self_calculated":self.hash(last_block)
                }

            # Check that the Proof of Work is correct
            if not self.valid_block(last_block['proof'], block['proof'], block['previous_hash']):
                return { 
                    "Error":"Invalid proof"
                }

            # check that the Merkle Root hash is valid
            if not self.valid_merkle_root(block['merkle_root'], block['transactions']):
                return False

            # move forward with the iteration if no erorrs found in current block
            last_block = block
            current_index += 1

        # if while loop exits then return True, otherwise an error was found in the chain
        return True


    def discover_POW_nodes(self):
        """ 
        Function to determine available servers in range 10.0.0.100 and up
        """
        # construct a list of strings that represent BCNode IP addresses
        servers = ["10.0.0.%s" % (h) for h in range(100, 120)]
        # check whats the current BCNode's IP to exclude it from checking
        own_ip = check_output(['hostname', '--all-ip-addresses']).strip('\n').strip(' ')
        # loop through and check each server if it responds to ping, then self register it
        for server_ip in servers:
                if server_ip != own_ip:
                        if os.system("timeout 0.1 ping -c1 " + server_ip) == 0:
                                self.register_node("http://"+server_ip+":5000")


    def valid_chain(self, chain):
        """
        Determine if a given blockchain is valid
        :param chain: A blockchain
        :return: True if valid, False if not
        """
        chain = chain["chain"]
        last_block = chain[0]
        current_index = 1

        # loop through the given BC
        while current_index < len(chain):
            block = chain[current_index]
           
            # check hash pointer to previous block
            if block['previous_hash'] != self.hash(last_block):
                return False

            # Check that the Proof of Work is correct
            if not self.valid_block(last_block['proof'], block['proof'], block['previous_hash']):
                return False

            # chek the merkle root hash validity
            if not self.valid_merkle_root(block['merkle_root'], block['transactions']):
                return False

            # progress in the iteration
            last_block = block
            current_index += 1

        # if looping finished without error return True to signal validity of given BC
        return True

    def proof_of(self, last_block):
        """
        Simple Proof of Work Algorithm:
         - Find a number p' such that hash(pp') contains leading 4 zeroes
         - Where p is the previous proof, and p' is the new proof

        :param last_block: <dict> last Block
        :return: <int>
        """
        last_proof = last_block['proof']
        last_hash = self.hash(last_block)

        # iterate an integer and hash it with the blocks until difficulty target is met
        proof = 0
        while self.valid_block(last_proof, proof, last_hash) is False:
            proof += 1

        # return integer once proof is found
        return proof

    def valid_block(self, last_proof, proof, last_hash):

        """
        Validates the Proof
        :param last_proof: <int> Previous Proof
        :param proof: <int> Current Proof
        :param last_hash: <str> The hash of the Previous Block
        :return: <bool> True if correct, False if not.
        """
        guess = '{}{}{}'.format(last_proof, proof, last_hash).encode()
        guess_hash = hashlib.sha256(guess).hexdigest()
        return guess_hash[:self.difficulty] == "0" * self.difficulty

    def full_chain(self):
        """
        Return full chain in to be jsonified later
        """
        response = {
            'chain': self.chain,
            'length': len(self.chain)

        }
        return response

    def resolve_conflicts(self, *args, **kwargs):
        """
        This is our consensus algorithm, it resolves conflicts
        by replacing our chain with the longest one in the network.
        :return: True if our chain was replaced, False if not
        """

        # loop prevention mechanism via passing {"path" : [list of processed NODE-IDs]}
        path = kwargs.get('path', None)
        if path is None:
            json = {"path": [self.node_identifier]} 
        else:
            if self.node_identifier in path:
                return False
            path.append(self.node_identifier)
            json = {"path": path}

        # loop through neighbors and compare their BC to ours to see if it's better (longer)
        neighbours = self.nodes
        better_chain = None

        # We're only looking for chains longer than ours
        max_length = len(self.chain)

        # Grab and verify the chains from all the nodes in our network
        for node in neighbours:
            response = requests.post('http://{}/chain'.format(node), json=json)

            # if response code is OK then let's check contents
            if response.status_code == 201:
                new_length = response.json()['length']
                new_chain = response.json()
                # Check if the length is longer and the chain is valid
                if new_length > max_length and self.valid_chain(new_chain):
                    max_length = new_length
                    better_chain = new_chain["chain"]

        # Replace our chain if we discovered a new, valid chain longer than ours
        if better_chain:
            self.chain = better_chain
            self.save_db()
            return True
        return False


class QuantumBlockChain(BaseBlockChain):
    """
    BlockChain with proof based on QKD (Quantum Key Distribution) machinery.
    """ 
    def __init__(self, app, node_identifier):
        super().__init__(node_identifier=node_identifier)
        # flask app to be extended
        self.app = app
        
        # address and port of this node's keyworker address
        self.keyworker = self.ip_address + ":55554"
        
        # convenience function to register neighbors automatically
        # self.discover_quantum_neighbors()

        @app.route('/keyworker/register', methods=['POST'])
        def register_keyworkers():
            """Function to register keyworkers via HTTP POST """
            values = request.get_json()
            if values is None:
                return "Error: Please supply a valid keyworker", 400
            
            # validate body JSON data
            kw_address = values.get('address')
            if kw_address is None:
                return "Error: Please supply a valid keyworker in JSON format", 400
            
            # try to register
            if self.register_keyworker(kw_address):
                    return "Error: Failed to add keyworker (Invalid address)\n\r", 400

            # return API response success
            response = {
                'message': 'New keyworker has been registered',
                'address': self.keyworker,
            }
            return jsonify(response), 200

        @app.route('/keyworkers', methods=['DELETE'])
        def clear_keyworkers():
            """ Function to delete ALL registered keyworkers """
            self.keyworker = ""
            return jsonify({'message': 'Keyworkers removed'})

        @app.route('/keyworkers', methods=['GET'])
        def get_keyworkers():
            """ Function to get list of all keyworkers """
            return jsonify({ "keyworker": self.keyworker })
 
        @app.route('/chain', methods=['POST'])
        def full_chain_post():
            """ Function to handle consensus with support for loop-prevention"""
            # parse JSON body values
            values = request.get_json()
            # return error if JSON is not valid
            if values is None:
                return "Wrong params", 400
            # If token that is passed around already has our node ID then skip, otherwise do our own resolve
            if self.node_identifier not in values.get("path"):
                self.resolve_conflicts(path=values.get("path"))
            # return chain data in JSON format
            return jsonify(self.full_chain_quantum(self.keyworker, request.remote_addr)), 201

    def register_keyworker(self, address):
        """
        Update the registered keyworker on current node!
        :param address: Address of node. Eg. 'http://localhost:55554'
        """
        parsed_url = urlparse(address)
        if parsed_url.netloc:
            _address = parsed_url.netloc
        elif parsed_url.path:
            _address = parsed_url.path
        else:
            return 1

        self.keyworker =  _address
        return 0

    def register_node(self, new_node):
        """
        Add a new node to the list of nodes
        :param new_node: Address and keyworker of node. Eg. '{"address":"http://127.0.0.1:5001","keyworker_id":0}'
        """
        _address = new_node.get("address")
        _channel_id = new_node.get("channel_id")

        # check node values to be registered
        if _address is None or _channel_id is None:  
            raise ValueError('Missing ADDRESS, KEYWORKER_ID or CHANNEL_ID for Node')

        # check address validiy
        parsed_url = urlparse(_address)
        if parsed_url.netloc:
            self.nodes.append({"address": parsed_url.netloc, "channel_id": _channel_id})
        elif parsed_url.path:
            self.nodes.append({"address": parsed_url.path, "channel_id": _channel_id})
        else:
            raise ValueError('Invalid URL')

    def remove_nodes(self):
        """ Clear nodes list """
        self.nodes = []

    def getlastkey(self, keyworker, remote_ip):
        """
        Function to get KEY from KEYWORKER for remote_ip which is used to identify quantum channel
        :param keyworker: keyworker address to work with
        :param remote_ip: IP to construct quantum channel ID to get key for
        """
        # make JSON formatted HTTP GET to the /getkey endpoint of keystore
        try:
            response = requests.get('http://{}/getkey'.format(keyworker), 
                                    json=dict(channel=self.determine_quantum_channel_id(self.ip_address, remote_ip)))
            response.raise_for_status()
        except:
            print("ERROR IN HTTP POST to /getkey")
            return 0
        
        # capture response if no errors occured
        resp_body_json = json.loads(response.text)

        # check if JSON response contains necessary fields
        if resp_body_json["key"] is None or resp_body_json["sha"] is None:
            return 0

        return {
            'key': base64.b64decode(resp_body_json["key"]).encode('hex'),
            'sha': resp_body_json["sha"]
        }

    def getkeybysha(self, key_sha, keyworker):
        """
        Function to get key from KEYWORKER by just referencing the SHA256 of the key
        :param key_sha: the sha256 value of the key that we want to retrieve
        :param keyworker: the target KW to which the request is sent
        """
        try:
            response = requests.get('http://{}/getkeybysha'.format(keyworker), 
                                        json=dict(sha=key_sha))
            response.raise_for_status()
        except:
            print("ERROR IN HTTP GET to /getkeybysha")
            return 0

        # capture response if no errors occured
        resp_body_json = json.loads(response.text)

        # check if JSON response contains necessary fields
        if resp_body_json["key"] is None or resp_body_json["sha"] is None:
            return 0

        return {
            'key': base64.b64decode(resp_body_json["key"]).encode('hex'),
            'sha': resp_body_json["sha"]
        }

    def valid_chain(self, chain, keyworker):
        """
        Determine if a GIVEN blockchain is valid
        :param chain: A blockchain
        :param keyworker: keyworker for work with
        :return: True if valid, False if not
        """
        # these two parameters are used to seal the BC that was given in arguments
        quantum_proof = chain["quantum_proof"]
        quantum_hash = chain["quantum_hash"]

        # variables for correct iteration
        chain = chain["chain"]
        last_block = chain[0]
        current_index = 1

        # loop through blockchain
        while current_index < len(chain):
            block = chain[current_index]
            # Check that the hash of the block is correct
            if block['previous_hash'] != self.hash(last_block):
                return False

            # Check that the Proof of Work is correct
            if not self.valid_block(last_block['proof'], block['proof'], block['previous_hash']):
                print("error in consistency")
                return False

            # chek the merkle root hash validity
            if not self.valid_merkle_root(block['merkle_root'], block['transactions']):
                return False

            last_block = block
            current_index += 1

        # get key from keystore that can be used to validate Quantum Proof on the given BC
        quantum_key = self.getkeybysha(quantum_hash, keyworker)

        # return error if key cannot be retrieved
        if quantum_key == 0:
            self.app.logger.info("NO SUCH KEY")
            return False

        # otherwise calculate the proof from given BC and comapre with given proof
        guess = '{}{}'.format(chain[-1]["proof"], quantum_key["key"]).encode()
        guess_proof = hashlib.sha256(guess).hexdigest()
        return guess_proof == quantum_proof

    def proof_of(self, last_block):
        """
        Simple Proof of QKD Algorithm:
         - Find a number p' such that hash(pp') contains leading 4 zeroes
         - Where p is the previous proof, and p' is the new proof
        :param last_block: <dict> last Block
        :return: <int>
        """
        # proof from last block
        last_proof = last_block['proof']
        
        # last block's hash value newly calculated
        last_hash = self.hash(last_block)
        
        # this proof just omits the random NONCE from PoW and hashes the proof from last block and its hash
        raw_proof = '{}{}'.format(last_proof, last_hash).encode()
        
        # proof is the HEX-digest of the above calculated value
        proof = hashlib.sha256(raw_proof).hexdigest()
        return proof

    def valid_block(self, last_proof, proof, last_hash):
        """
        Validates the Proof
        :param last_proof: <int> Previous Proof
        :param proof: <int> Current Proof
        :param last_hash: <str> The hash of the Previous Block
        :return: <bool> True if correct, False if not.
        """
        guess = '{last_proof}{last_hash}'.format(last_proof=last_proof, last_hash=last_hash).encode()
        guess_hash = hashlib.sha256(guess).hexdigest()
        return guess_hash == proof

    def full_chain_quantum(self, keyworker, remote_ip):
        """ Function that takes the current key from QKD and seals the current BC with that key """
        
        # get most recent key from QKD corresponding to neighbor IP (to distinguish channels)
        quantum_key = self.getlastkey(keyworker, remote_ip)
        
        # calculate proof
        guess = '{}{}'.format(self.chain[-1]["proof"], quantum_key["key"]).encode()
        guess_proof = hashlib.sha256(guess).hexdigest()
        
        # return sealed BC as dictionary (converted to JSON later)
        return {
            'chain': self.chain,
            'length': len(self.chain),
            'quantum_hash': quantum_key['sha'],
            'quantum_proof': guess_proof
        }

    def full_chain(self):
        """ Returns the full blockchain as a dictionary"""
        return {
            'chain': self.chain,
            'length': len(self.chain)
        }

    def resolve_conflicts(self, *args, **kwargs):
        """
        This is our consensus algorithm, it resolves conflicts
        by replacing our chain with the longest one in the network.
        :return: True if our chain was replaced, False if not
        """   

        # loop prevention by passing list of node IDs in json token {"path": [list]}
        path = kwargs.get('path', None)
        if path is None:
            json = {"path": [self.node_identifier]} 
        else:
            if self.node_identifier in path:
                return False
            path.append(self.node_identifier)
            json = {"path": path}

        # variables for looping through each neighbor and comparing their chain with ours
        neighbours = self.nodes
        better_chain = None

        # We're only looking for chains longer than ours
        max_length = len(self.chain)

        # Grab and verify the chains from all the nodes in our network
        for node in neighbours:
            # grab the chain from each neighbor, and put in payload the JSON path
            response = requests.post('http://{}/chain'.format(node["address"]), json=json)

            # if response code is ok, then compare chains
            if response.status_code == 201:
                new_length = response.json()['length']
                new_chain = response.json()
                # Check if the length is longer and the chain is valid
                if new_length > max_length and self.valid_chain(new_chain, self.keyworker):
                    max_length = new_length
                    better_chain = new_chain["chain"]
        
        # Replace our chain if we discovered a new, valid chain longer than ours
        if better_chain is None:
            return False
        self.chain = better_chain
        self.save_db()
        return True

    def determine_quantum_channel_id(self, firstIP, secondIP):
        """ Helper function to create the channel ID from two given IP addresses in format 'X-X' """
        
        # get the last octet from both IPs
        firstIPLastOctet = firstIP.split(".")[3]
        secondIPLastOctet = secondIP.split(".")[3]

        # determine their ordering to know which one to put first
        if int(firstIPLastOctet) < int(secondIPLastOctet):
            return firstIPLastOctet + "-" + secondIPLastOctet
        return secondIPLastOctet + "-" + firstIPLastOctet


    def discover_quantum_neighbors(self):
        """ Function to determine available servers in range 10.0.0.100 and up """
        servers = ["10.0.0.%s" % (h) for h in range(100, 120)]

        # loop through servers and ping them then construct channel ID if alive
        for server_ip in servers:
            if server_ip != self.ip_address:
                if os.system("timeout 0.1 ping -c1 {} >/dev/null 2>&1".format(server_ip)) == 0:
                        self.register_node({
                            "address":"http://"+server_ip+":5000",
                            "channel_id": self.determine_quantum_channel_id(self.ip_address, server_ip)
                        })

    def is_chain_valid(self):
        """ Function to determine validity of our own chain"""
        all_nodes = ["10.0.0.100","10.0.0.101","10.0.0.102","10.0.0.103"]
        remote_ip = [ ip for ip in all_nodes if ip != self.ip_address]
        chain = self.full_chain_quantum(self.keyworker, remote_ip[0])
        return self.valid_chain(chain, self.keyworker)