# Block[Chain] Alchemy Laboratory - SWM Modelling Framework

Extension of BAL framework for supporting my master thesis work.

## Mininet Scenario 1 [POW]:

![alt text](files/MininetScenario1.jpg)

The basic idea for this scenario is to create a mininet topology with 9 Smart Garbage Bins grouped into 3 districts and a central district with 3 Blockchain nodes/servers belonging to the Waste Management Operation Center. Each block of 3 SGBs are assigned to send their requests to one of the 3 servers (in a deterministic manner) that correspond to their region.

Once the topology is established each node is going to start an application according to its functionality.
* sgb1-9 nodes will run in a loop sending HTTP POST requests to one of the 3 blockchain nodes
* srv1-3 will start a flavour of blockchain application and listen to the incoming API requests 

List of TODOs and ideas:
* ~~implement a form of registry that can map hostnames to UUID (eg: "sgb1": "abc7868fbcdab7761367cbdfa78")~~
* ~~implement a limit on max current outstanding transactions~~ 
* ~~implement and add merkle root of TX-es?~~
* ~~implement a helper/coordinator for convenience functionalities (sending of /nodes/register to all 3 server nodes)~~
* ~~implement automatic discovery of BCNodes starting from IP 10.0.0.100 and up~~
* ~~implement automatic mining on a scheudled basis? (every time outstanding TX-es reach 10 mining is called)~~
* ~~fixed a bug that corrupted the hash-links in the chain (related to submission of new TX-es while mining is in progress)~~
* ~~added deterministic node_id to blockchain nodes instead of randomized one~~
* ~~small change in BaseBlockchain nodes variable: changed from set to list to make it consistent~~
* ~~update POW scenario to use 4 bc nodes and 4 groups of SGB Clients~~
* ~~implemented QUANT style propagation of consensus from one node to another~~
* improve consensus in a way that only chain_length numbers are queried and only longest is actually downloaded
* randomize sending of new TX-es to different bc nodes? OR implement redistribution of TX-es by BCNodes themselves

## Mininet Scenario 2 [Quantum]:

![alt text](files/MininetScenario2.jpg)

The idea for this scenario is to create a similar topology as seen in V1 with PoW blockchain, but use Quantum Consensus from BAL. 

Once the topology is established each node is going to start an application according to its functionality.
* The SGB emulator nodes `sgb1-12` run in a loop sending new BC Transactions to one of the 4 quantum blockchain nodes with random delays
* The BCNodes `srv1-4` run the QUANT flavour of blockchain application (the corresponding KW instance is automatically registered, and other BCNODE neighbors are discovered automatically)
* every srv node will also have a keyworker instance running which are responsible for receiving keys from the QKD Emulator and storing it for use by the blockchain application
* The QKD Emulator node runs several instances of `qkdemu`, each emulating a real-world Quantum Key Distribution unit. Two keyworker instances that share the same segment will belong to the same `qkdemu` instance and receive the same key periodically, just as it they connected to a real QKD equipment.

 interact with KW instances by generating all possile pairs and installing the same key in the KW of each memeber of each pair. For example, by default there are 4 blockchain nodes, that gives `4*3/2 = 6` possible pairs, and the QKDE will create these pairs and send a new key to each KW belonging to each pair.

 TODO:
 * ~~improve KEYWORKER to handle multiple QKDEMU nodes simultaneously~~
 * improve QUANT consensus to get key from KW for the correct node that called consensus
 * ~~implement channel_id on keystore and quant variants to distinguish between quantum channels (Channel ID concept: take the last octet of the node's IP Address and put them together, example: IP1: 10.0.0.100, IP2: 10.0.0.102 > ChannelID: 100-102. Always make sure to put the smaller number first!)~~


## More information on BAL:
you may find in the project's [Wiki](https://gitlab.com/BAlchemyLab/bal/wikis/home).
